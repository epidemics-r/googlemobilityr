# Install

First make sure you have the devtools package install in R by executing the following in R:

```{r}
install.packages("devtools")
```

Then use devtools to install `googleMobilityR` by using the following command:

```{r}
library(devtools)
install_git("https://gitlab.com/epidemics-r/googlemobilityr")
```

# Usage

The package downloads the latest google mobility data and allows you to extract data by location code etc. For example typical usage to get google mobility data from Liverpool city:

```{r}
library("googleMobilityR")

# Get the date for GB
id <- download_data()
gb_df <- extract_country(id, "GB")

# Get data for Liverpool (using LAD code)
liverpool_df <- extract_by_code(gb_df, code = "E08000012")

# Smooth it in various ways
df1 <- smooth(data = liverpool_df, method = "week")
df2 <- smooth(data = liverpool_df, method = "rolling") 

library(ggplot2)
ggplot(data = df2) + geom_line(aes(x = Date, y = value, colour = GPL_TYPE))
```

Often you want data not just for one local authority but from multiple. If you pass multiple codes to `extract_by_code` it will average the scores by population size:


```{r}
library("googleMobilityR")

# Get the date for GB
id <- download_data()
gb_df <- extract_country(id, "GB")

# Get data for Liverpool and Knowsley (using LAD code)
df <- extract_by_code(gb_df, code = c("E08000012", "E08000011"))

# Smooth it in various ways
df1 <- smooth(data = df, method = "week")
library(ggplot2)
ggplot(data = df1) + geom_line(aes(x = Date, y = value, colour = GPL_TYPE))

df2 <- smooth(data = df, method = "rolling") 
```

